import React, {Component} from 'react';
import {AppRegistry, Image, StyleSheet, Text, View, ListView, TextInput} from 'react-native';


export default class HelloReact extends Component {

    /**
     * Construction method for setting initial values.
     * @param props
     */
    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (row1, row2) => row1 !== row2,
            })
        };
    }

    /**
     * Actual API calling function.
     *
     * @apiPath original Github api endpoint for fetching repositories
     * @param text: query to be searched
     */
    fetchData(text) {
        let apiPath = `https://api.github.com/search/repositories?q=${text}&sort=stars&order=desc`;
        fetch(apiPath, {method: 'GET'})
            .then((response) => response.json())
            .then((responseData) => {
                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(responseData.items)
                });
            }).catch((err) => {
                console.log(err);
            })
            .done();
    }

    /**
     * handles the text inputted from the search field and sends it to API
     * @param text
     */
    handleSearchInput(text) {
        if(text !== '') this.fetchData(text);
    }

    /**
     * React Render function
     * @returns {*}
     */
    render() {
        return (
            <View>
                <TextInput onChangeText={(text) => this.handleSearchInput(text)} value={this.state.text}/>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={this.renderList}
                    style={styles.listView}/>
            </View>
        );
    }


    /**
     * Single item of the Results list
     * @param repo
     * @returns {XML}
     */
    renderList(repo) {
        return (
            <View style={styles.container}>
                <Image source={{uri: repo.owner.avatar_url}} style={styles.thumbnail}/>

                <View style={styles.rightContainer}>
                    <Text style={styles.title}>{repo.name}</Text>
                    <Text style={styles.year}>@{repo.owner.login}</Text>
                </View>
            </View>
        );
    }
}

/**
 * Basic Styles for views
 */
let styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    thumbnail: {
        width: 53,
        height: 81,
    },
    rightContainer: {
        flex: 1
    },
    title: {
        fontSize: 20,
        marginBottom: 8,
        paddingRight: 20,
        textAlign: 'left',
    },
    year: {
        paddingRight: 20,
        textAlign: 'left',
    },
    listView: {
        paddingTop: 20,
        backgroundColor: '#F5FCFF',
    },
});

AppRegistry.registerComponent('HelloReact', () => HelloReact);
